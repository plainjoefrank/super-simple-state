import uuidv4 from 'uuid/v4';

class StateService
{
    constructor()
    {
        const storeSubscribers = [];  
        const previousState = [];
        const currentState = [];
        const updateControl = []
    
        this.subscribe = function(storeName, subscriber)
        {
            var subscriberId = uuidv4();
            const subscriberWithId = {subscriberId: subscriberId, subscriber: subscriber};

            var subscribers = storeSubscribers[storeName];
            if (subscribers === undefined)
            {
                storeSubscribers[storeName] = [subscriberWithId];
            }
            else
            {
                subscribers.push(subscriberWithId);
            }
            return subscriberId;
        }

        this.update = function(storeName, nextState) 
        {
            previousState[storeName] = currentState[storeName];
            currentState[storeName] = nextState;

            if(updateControl[storeName] === undefined)
            {
                updateControl[storeName] = false;
            }
            
            const isUpdating = updateControl[storeName];
        
            if(isUpdating === false)
            {
                var subscribers = storeSubscribers[storeName];
                if (subscribers !== undefined)
                {
                    subscribers.map((subscriberWithId) => 
                    {
                        //enforce immutability
                        var clonedPreviousState = this.getPreviousState(storeName);
                        var clonedCurrentState = this.getCurrentState(storeName);

                        return subscriberWithId.subscriber(clonedPreviousState, clonedCurrentState);
                    });
                }
                else
                {
                    console.log("subscriber store not found", storeName);
                }
            }
            else
            {
                console.warn("State update honored, but not pushed to subscribers to prevent recursion.");
            }
        }

        this.unsubscribe = function(storeName, subscriberId)
        {
            var subscribers = storeSubscribers[storeName];
            subscribers.map((subscriberWithId, index, originalSubscribers) => 
            {
                if(subscriberWithId.subscriberId === subscriberId)
                {
                    originalSubscribers.splice(index, 1);
                }

                return true;
            });
        }

        this.getPreviousState = function(storeName)
        {
            var clonedPreviousState = {};
            if(Object.keys(previousState).indexOf(storeName) >= 0 && previousState[storeName] !== undefined)
            {
                clonedPreviousState = JSON.parse(JSON.stringify(previousState[storeName]));
            }
            return clonedPreviousState;      
        }

        this.getCurrentState = function(storeName)
        {
            var clonedCurrentState = {};
            if(Object.keys(currentState).indexOf(storeName) >= 0 && currentState[storeName] !== undefined)
            {
                clonedCurrentState = JSON.parse(JSON.stringify(currentState[storeName]));
            }
            return clonedCurrentState;
        }
    }
}

export default new StateService();