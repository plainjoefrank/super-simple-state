import React, { Component } from 'react';
import StateService from './super-simple-state';

class Color extends Component
{
  constructor(props)
  {
    super(props);

    this.onColor = props.onColor;
    this.offColor = props.offColor;
    this.currentColor = this.offColor;

    //set the initial state
    var nextColorState = StateService.getCurrentState("ColorStore");
    nextColorState.isOn = false
    StateService.update("ColorStore", nextColorState);
  }

  componentWillMount()
  {
    this.colorStoreSubscriberId = StateService.subscribe("ColorStore", (previousState, currentState) => 
    {
      if (previousState.isOn !== currentState.isOn)
      {
        if (currentState.isOn === true)
        {
          this.currentColor = this.onColor;
        }
        else
        {
          this.currentColor = this.offColor;
        }
        //cause page refresh the react way
        this.setState({ refreshDate: new Date() });
      }
    });
  }

  componentWillUnmount()
  {
    StateService.unsubscribe("colorStore", this.colorStoreSubscriberId);
  }

  render()
  {
    const style =
    {
      backgroundColor: this.currentColor,
      height: '30px',
      width: '30px'
    }
    return (<div style={style}></div>)
  }
}

export default Color;
