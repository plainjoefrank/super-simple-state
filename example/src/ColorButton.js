import React, { Component } from 'react';
import StateService from './super-simple-state';
import { NONAME } from 'dns';

class ColorButton extends Component
{
  constructor(props)
  {
    super(props);

    this.buttonText = "OFF";

    //set the initial state
    var nextColorState = StateService.getCurrentState("ColorStore");
    nextColorState.isOn = false;
    StateService.update("ColorStore", nextColorState);
  }

  componentWillMount()
  {
    this.colorStoreSubscriberId = StateService.subscribe("ColorStore", (previousState, currentState) => 
    {
      if (previousState.isOn !== currentState.isOn)
      {
        if(currentState.isOn === true)
        {
          this.buttonText = "ON";
        }
        else
        {
          this.buttonText = "OFF";
        }
        //cause page refresh the react way
        this.setState({ refreshDate: new Date() });
      }
    });
  }

  componentWillUnmount()
  {
    StateService.unsubscribe("colorStore", this.colorStoreSubscriberId);
  }

  toggle(event)
  {
    var nextColorState = StateService.getCurrentState("ColorStore");
    nextColorState.isOn = (nextColorState.isOn === false);
    StateService.update("ColorStore", nextColorState);
  }

  render()
  {
    const style =
    {
      backgroundColor: 'darkBlue',
      color: 'white', 
      height: '50px', 
      width: '250px', 
      cursor: 'pointer', 
      lineHeight: '2em',
      userSelect: 'none'
    }
    return (<div style={style} onClick={(e) => this.toggle()}>{this.buttonText}</div>)
  }
}

export default ColorButton;
