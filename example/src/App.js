import React, { Component } from 'react';
import './App.css';
import Color from './Color'
import ColorButton from './ColorButton'

class App extends Component
{
  render()
  {
    return (
      <div className="App">
        <header className="App-header">
          <Color onColor='red' offColor='black'></Color>
          <Color onColor='blue' offColor='black'></Color>
          <Color onColor='green' offColor='black'></Color>
          <p>Super Simple State Example</p>
          <ColorButton></ColorButton>
        </header>
      </div>
    );
  }
}

export default App;
